
package Ventas;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import proyecto.seminario.Interfaz;
import proyecto.seminario.conexion;

public final class AreaVentas extends javax.swing.JFrame {

    ArrayList<VentaObject> ls = new ArrayList();
    int idProducto;
    double precio;
    String nombre;
    
    public AreaVentas() {
        initComponents();
        LoadInfo();
        buscarTF.addKeyListener(new KeyListener(){
            @Override
            public void keyTyped(KeyEvent arg0) {
                
            }

            @Override
            public void keyPressed(KeyEvent arg0) {
                if(arg0.getKeyCode() == KeyEvent.VK_ENTER){
                    String id = buscarTF.getText();
                    LoadInfo(id);
                }
            }

            @Override
            public void keyReleased(KeyEvent arg0) {
                
            }
            
        });
    }
    
    void LoadInfo(String id){
        if("".equals(id)){
            LoadInfo();
            canTF.setText("");
        } else {
            try{
                String titulos[] = {"ID","Producto","Precio","Cantidad"};
                String product[] = new String[4];
                String sql = "SELECT * FROM producto WHERE idProducto = '" + id + "';";
                DefaultTableModel model = new DefaultTableModel(null,titulos);
                Connection con = conexion.conectar();
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);
                while(rs.next()){
                    product[0] = Integer.toString(rs.getInt("idProducto"));
                    product[1] = rs.getString("nombreProducto");
                    product[2] = Double.toString(rs.getDouble("precioProducto"));
                    product[3] = Integer.toString(rs.getInt("cantidadProducto"));
                    model.addRow(product);
                }
                tabla.setModel(model);
                TableColumnModel column = tabla.getColumnModel();
                column.getColumn(3).setPreferredWidth(5);
                column.getColumn(2).setPreferredWidth(5);
                column.getColumn(0).setPreferredWidth(20);
                tabla.getTableHeader().setResizingAllowed(false);
                canTF.setText(product[3]);
                idProducto = Integer.parseInt(product[0]);
                nombre = product[1];
                precio = Double.parseDouble(product[2]);
            } catch(SQLException e){
                JOptionPane.showMessageDialog(null,"Error de conexión: " + e);
            }
        }
    }
    
    void LoadInfo(){
        try{
            String titulos[] = {"ID", "Producto","Precio","Cantidad"};
            String product[] = new String[4];
            String sql = "SELECT * FROM producto;";
            DefaultTableModel model = new DefaultTableModel(null,titulos);
            Connection con = conexion.conectar();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                product[0] = Integer.toString(rs.getInt("idProducto"));
                product[1] = rs.getString("nombreProducto");
                product[2] = Double.toString(rs.getDouble("precioProducto"));
                product[3] = Integer.toString(rs.getInt("cantidadProducto"));
                model.addRow(product);
            }
            tabla.setModel(model);
            TableColumnModel column = tabla.getColumnModel();
            column.getColumn(3).setPreferredWidth(5);
            column.getColumn(2).setPreferredWidth(5);
            column.getColumn(0).setPreferredWidth(20);
            tabla.getTableHeader().setResizingAllowed(false);
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null,"Error de conexión: " + e);
        }
    }

    void LoadList(Double precio){
        
        DefaultListModel model = new DefaultListModel();
        for(int i = 0; i <ls.size(); i++){
            model.add(i,ls.get(i).toString());
        }
        lista.setModel(model);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        buscarTF = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lista = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        canTF = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        can2TF = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabla.setEnabled(false);
        jScrollPane2.setViewportView(tabla);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 511, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(lista);

        jLabel1.setText("Cantidad:");

        canTF.setEnabled(false);

        jButton1.setText("jButton1");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton3.setText("Limpiar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(canTF, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addComponent(can2TF, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton1)))
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(canTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(can2TF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addComponent(jButton3))
        );

        jButton2.setText("Pagar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buscarTF))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(15, 15, 15))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(71, 71, 71)
                .addComponent(buscarTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(21, 21, 21)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public boolean isExist(int v){
        for(int i = 0; i < ls.size(); i++){
            if(ls.get(i).getIdProduct() == v){
                System.out.println("Es igual");
                return true;
            }
        }
        return false;
    }
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int num1 = Integer.parseInt(canTF.getText());
        int num2 = Integer.parseInt(can2TF.getText());
        
        if(num2 <= num1 && num2 > 0){
            canTF.setText(Integer.toString(num1 - num2));
            Double p = precio * num2;
            if(isExist(idProducto)){
                for(int i = 0; i < ls.size(); i++){
                    if(ls.get(i).getIdProduct() == idProducto){
                        ls.get(i).setMonto(ls.get(i).getMonto() + p);
                        ls.get(i).setCantidad(ls.get(i).getCantidad() + num2);
                    }
                }
            } else {
                VentaObject v = new VentaObject();
                v.setIdProduct(idProducto);
                v.setName(nombre);
                v.setMonto(p);
                v.setCantidad(num2);
                ls.add(v);
            }
            LoadList(p);
        } else {
            JOptionPane.showMessageDialog(null,"Número no valido, escriba uno menor al que hay en stock.");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try{
            String sql = "INSERT INTO ventas(cantidadVenta,empleadoVenta,fechaVenta) VALUES(?,?,?);";
            double mount = 0;
            for(int i = 0; i < ls.size(); i++){
                mount += ls.get(i).getMonto();
            }
            int idUs = Interfaz.usId;
            Date dia = new Date();
            java.sql.Date d = new java.sql.Date(dia.getTime());
            Connection con = conexion.conectar();
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setDouble(1, mount);
            ps.setInt(2, idUs);
            ps.setDate(3, d);
            int res = ps.executeUpdate();
            if(res == 0){
                JOptionPane.showMessageDialog(null,"No fue posible realizar la compra.");
            } else {
                ps.close();
                con.close();
                Connection con2 = conexion.conectar();
                int id = getLastId();
                System.out.println("Llego al for");
                for(int i = 0; i < ls.size(); i++){
                    String sql2 = "INSERT INTO detallesVenta(idV,idP,cantidad,fecha)"
                            + " VALUES(?,?,?,?)";
                    PreparedStatement ps2 = con2.prepareStatement(sql2);
                    ps2.setInt(1, id);
                    ps2.setInt(2, ls.get(i).getIdProduct());
                    ps2.setInt(3,ls.get(i).getCantidad());
                    ps2.setDate(4, d);
                    int res2 = ps2.executeUpdate();
                    if(res2 == 0){
                        JOptionPane.showMessageDialog(null,"No fue posible realizar la compra.");
                    } else {
                        updateDataBase(ls.get(i).getIdProduct(), ls.get(i).getCantidad());
                    }
                }
                JOptionPane.showMessageDialog(null,"Compra realizada con exito!");
                ls.clear();
                LoadInfo();
                buscarTF.setText("");
                canTF.setText("");
                can2TF.setText("");
                DefaultListModel model = (DefaultListModel)lista.getModel();
                model.clear();
            }
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null,"Error de conexión: " + e);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        buscarTF.setText("");
        canTF.setText("");
        can2TF.setText("");
        ls.clear();
        DefaultListModel model = (DefaultListModel)lista.getModel();
        model.clear();
        LoadInfo();
    }//GEN-LAST:event_jButton3ActionPerformed

    public int getLastId(){
        int id = 0;
        try{
            String sql = "SELECT * FROM ventas ORDER BY idVenta DESC LIMIT 1;";
            Connection con = conexion.conectar();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if(rs.next()){
                id = rs.getInt("idVenta");
            } else {
                JOptionPane.showMessageDialog(null,"No fue posible realizar la compra.");
            }
            System.out.println(id);
            return id;
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null,"Error de conexión: " + e);
        }
        return 0;
    }
    
    public void updateDataBase(int id, int cantidad){
        try{
            String sql = "UPDATE producto SET cantidadProducto = cantidadProducto - ? WHERE idProducto = ?;";
            Connection con = conexion.conectar();
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, cantidad);
            ps.setInt(2, id);
            int res = ps.executeUpdate();
            if(res == 0){
                JOptionPane.showMessageDialog(null,"Error en la actualización de la base, pongase en contacto con el administrador.");
            }
        } catch(SQLException e){
            
        }
    }
    
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AreaVentas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField buscarTF;
    private javax.swing.JTextField can2TF;
    private javax.swing.JTextField canTF;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList<String> lista;
    private javax.swing.JTable tabla;
    // End of variables declaration//GEN-END:variables
}
